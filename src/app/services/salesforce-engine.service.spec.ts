import { TestBed, inject } from '@angular/core/testing';

import { SalesforceEngineService } from './salesforce-engine.service';

describe('SalesforceEngineService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SalesforceEngineService]
    });
  });

  it('should be created', inject([SalesforceEngineService], (service: SalesforceEngineService) => {
    expect(service).toBeTruthy();
  }));
});
