import { Component, OnInit } from '@angular/core';
import { SalesforceEngineService } from '../../services/salesforce-engine.service';

@Component({
  selector: 'account-list',
  templateUrl: './account-list.component.html',
  styleUrls: ['./account-list.component.css']
})
export class AccountListComponent implements OnInit {
  accountList: any[];
  constructor(private _salesforce: SalesforceEngineService) { }

  ngOnInit() {
    var response = this._salesforce.invokeAction('AngularDemoController.getAccounts',[]);
    response.subscribe((data:any)=>{this.updateUI(data.result, data.event)},this.ExceptionHandler);
  }

  updateUI(result, event){
    this.accountList = result;
  }
  ExceptionHandler(data){
    alert(data);
  }
}
