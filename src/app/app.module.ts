import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SalesforceEngineService } from './services/salesforce-engine.service';
import { AppComponent } from './app.component';
import { AccountListComponent } from './components/account-list/account-list.component';

@NgModule({
  declarations: [
    AppComponent,
    AccountListComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [SalesforceEngineService],
  bootstrap: [AppComponent]
})
export class AppModule { }
